# SQL 
SQL or Structured Query Language is a domain-specific language used in programming and designed for managing data held in a **relational database management system** (RDBMS), or for stream processing in a relational data stream management system (RDSMS). It is particularly useful in handling structured data, i.e. data incorporating relations among entities and variables.

SQL has a ANSI Standard, yet there are different versions of the SQL language, however to be compliant with the ANSI standard, they all support at least the major commands, such as SELECT, UPDATE, DELETE, INSERT, WHERE, in a similar manner.

## RDBMS
**relational database management system**, RDBMS is the basis for SQL, and for all modern database systems such as MS SQL Server, IBM DB2, Oracle, MySQL, and Microsoft Access. The data in RDBMS is stored in database objects called tables. A table is a collection of related data entries and it consists of columns and rows. Every table is broken up into smaller entities called fields.
```sql
 SELECT * FROM Customers; 
```

The fields in the Customers table consist of CustomerID, CustomerName, ContactName, Address, City, PostalCode and Country. A field is a column in a table that is designed to maintain specific information about every record in the table. A record, also called a row, is each individual entry that exists in a table. For example, there are 91 records in the above Customers table. A record is a horizontal entity in a table. A column is a vertical entity in a table that contains all information associated with a specific field in a table.

---

# SQL Syntax

## Database Tables

A database most often contains one or more tables. Each table is identified by a name (e.g. "Customers" or "Orders"). Tables contain records (rows) with data. In this tutorial we will use the well-known Northwind sample database (included in SQLite3 and MySQL Server). The table below contains five records (one for each customer) and seven columns (CustomerID, CustomerName, ContactName, Address, City, PostalCode, and Country).

CustomerID| CustomerName| ContactName     | Address  | City  |	PostalCode|	Country
--------- | ----------- | --------------- | -------- | ----- | ---------- | ----------
1	| Alfreds Futterkiste | Maria Anders | Obere Str. 57 | 	Berlin |	12209 |	Germany
2 | Ana Trujillo Emparedados y helados | Ana Trujillo |	Avda. de la Constitución 2222 |	México D.F. | 05021 | Mexico
3 |	Antonio Moreno Taquería |	Antonio Moreno 	| Mataderos  2312 	| México D.F. |05023 |	Mexico
4 |Around the Horn |	Thomas Hardy |	120 Hanover Sq. |	London | 	WA1 1DP |	UK
5 | Berglunds snabbköp | Christina Berglund | Berguvsvägen 8 |	Luleå |	S-958 22 	| Sweden

## SQL Statements

Most of the actions you need to perform on a database are done with SQL statements.

The following SQL statement selects all the records in the "Customers" table:
Example

```sql
SELECT * FROM Customers;
```
> *_NOTE_*  SQL keywords are NOT case sensitive: select is the same as SELECT, it is generally good practice

## Semicolon
Some database systems require a semicolon at the end of each SQL statement. Semicolon is the standard way to separate each SQL statement in database systems that allow more than one SQL statement to be executed in the same call to the server.

## Some of SQL Commands:

- SELECT - extracts data from a database
- UPDATE - updates data in a database
- DELETE - deletes data from a database
- INSERT INTO - inserts new data into a database
- CREATE DATABASE - creates a new database
- ALTER DATABASE - modifies a database
- CREATE TABLE - creates a new table
- ALTER TABLE - modifies a table
- DROP TABLE - deletes a table
- CREATE INDEX - creates an index (search key)
- DROP INDEX - deletes an index


# SQLite
SQLite is a relational database management system (RDBMS) contained in a C library, also implemented in various other languages. In contrast to many other database management systems, SQLite is not a client–server database engine. Rather, it is embedded into the end program. 
